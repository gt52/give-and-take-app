export { default as HomeScreen } from "./HomeScreen";
export { default as PostScreen} from "./PostScreen";
export { default as Hardcopy} from "./Hardcopy";
export { default as Softcopy} from "./Softcopy";
export { default as Slide} from "./Slide";