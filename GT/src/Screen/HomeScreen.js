import React from "react";
import Header from "../components/header";
import Background from "../components/Background";
import Button from "../components/button";
import Slide from "./Slide";
import { 
  StyleSheet, 
  Text, 
  Dimensions,
  View,
  StatusBar,
  SafeAreaView,
  Image,
  FlatList,

} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";




export default function HomeScreen({navigation}){
  const products = [
    {
      id: 1,
     
      Image: require('../../assets/year/year1.png'),

    },
    {
      id: 2,
      
      Image: require('../../assets/year/year2.png'),
      

    },
    {
      id: 3,
      name : 'shark',
      Image: require('../../assets/year/year3.png'),
      

    },
    {
      id: 4,
      
      Image: require('../../assets/year/year4.png'),

    },
    
  ];
  
  const oneProduct =({item}) => (
    <View style={styles.item}>
      <View style={styles.yearContainer}>
        <Image source ={item.Image} style ={styles.year}/>
        
        
      <TouchableOpacity
      style={styles.btnstyle}
      onPress={() =>navigation.navigate('Hardcopy')}
      >
        <Text style={{color: 'white', textAlign:'center'}}>HardCopy</Text>
        
      </TouchableOpacity>
      <TouchableOpacity
      onPress={() =>navigation.navigate('Softcopy')}
      style={styles.btnstyle}
      >
        <Text style={{color: 'white',textAlign:'center'}}>Softcopy</Text>
      </TouchableOpacity>
        
      </View>
      
      
    </View>
    
  )
  itemSepartor = () => {
    return <View style={styles.separator} />
  }
   
  return (
 
  <SafeAreaView style={styles.container}>
     <Slide></Slide>
    <FlatList
    style={styles.FlatList}
    ListHeaderComponentStyle={styles.listHeader}
    data ={products}
    renderItem= {oneProduct}
    ItemSeparatorComponent ={itemSepartor}
    horizontal={false}
    numColumns={2}
    
    ></FlatList> 
    
    
  </SafeAreaView>
)
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center'  
  },
  FlatList:{
    width:'100%',
    padding:'7%',
  
    
  },
  
  separator: {
    height:1,
    width: '100%',
    backgroundColor: '#CCC',
  },
  listHeader: {
    height:55,
    alignItems:'center',
    justifyContent:'center',
  },
  item: {
    flex:1,
    flexDirection:'row',
    alignItems:'center',
    paddingVertical:13,
  },
  yearContainer: {
    backgroundColor:'#A3C4ED',
    borderRadius:8,
    height:270,
    width:159,
    justifyContent:'center',
    alignItems:'center',
    margin:'1.5%',
    
  },
  year:{
    height:110,
    width:140,
  },
  name: {
    fontWeight:'600',
    fontSize:16,
    marginLeft:13,
    color:'white',
  },
  btnstyle: {
    backgroundColor: '#037272',
    borderRadius:8,
    borderColor:'red',
    width:135,
    height:30,
    margin:'2%',
    
    
  }
 
});
