import { Component } from 'react';
import "react-native-gesture-handler"
import { StyleSheet,  Text, View, Image } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/components/button';
import TextInput from './src/components/TextInput';
import Header from './src/components/header';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {HomeScreen, PostScreen, Hardcopy,Softcopy} from './src/Screen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Drawerjs from './src/components/DrawerContent';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName='HomeScreen'
          screenOptions={{headerShown:false}}>
          <Stack.Screen name='HomeScreen'  component={DrawerNavigator} />
          <Stack.Screen name='Hardcopy'  component={Hardcopy} />
          <Stack.Screen name='Softcopy'  component={Softcopy} />
          
        </Stack.Navigator>
      </NavigationContainer>

    </Provider>
  );
}

function BottomNavigation(){
  return (
    <Tab.Navigator  style={styles.tabColor}>
      <Tab.Screen 
     
      name='Home' 
      component={HomeScreen} 
      options={{
        headerShown:false,
        tabBarIcon: ({size}) => {
          return(
            <Image
              style={{width: size, height: size}}
              source={
                require('./assets/home.png')}
            />
          );
        },

      }}
      />
      <Tab.Screen name='Post' component={PostScreen}
      options={{
        tabBarIcon: ({size}) => {
          return(
            <Image
              style={{width: size, height: size}}
              source={
                require('./assets/post.jpg')}
            />
          );
        },
        headerShown:false
      }}
      
      />
    </Tab.Navigator>
  )
}


const DrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={Drawerjs}>
      <Drawer.Screen name = 'GT' component={BottomNavigation} />
    </Drawer.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabColor: {
    color: '#3561F5',
  }
});
