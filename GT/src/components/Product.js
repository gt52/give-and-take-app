import React from "react";
import { View, Text, Image} from 'react-native'
import Button from "./button";

class Product extends React.Component{
    
    render(){
        const{ name, brand, price, productImage, button} = this.props.product
        return(
            <View style={{width:179,height:240, alignItems:'center', borderWidth: 0.75, margin:'1%', padding:10}}>
                <Image style={{width:150, height:150 }} source={{uri: productImage}} />
                <Text style={{textAlign:'center', fontWeight:'bold'}}>{name}</Text>
                <Text>{brand}</Text>
                <Text>{price}</Text>
                
            </View>
        )
    }
}
export default Product;