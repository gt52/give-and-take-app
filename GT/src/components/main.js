import React from "react";
import Header from "../components/header";
import Background from "../components/Background";
import Button from "../components/button";
import Slide from "./Slide";
import { 
  StyleSheet, 
  Text, 
  Dimensions,
  View,
  StatusBar,
  SafeAreaView,
  Image,
  FlatList,

} from 'react-native';
import Product from '../components/Product';

const StaticProductImage = 'https://share.sketchpad.app/22/dd1-31be-a35c7a.png';
const StaticProductImage1 = 'https://share.sketchpad.app/22/6ce-a84f-18c925.png';
const StaticProductImage2 = 'https://share.sketchpad.app/22/a91-92b3-8bf090.png';
const StaticProductImage3 = 'https://share.sketchpad.app/22/671-b367-f4b63a.png';

const products =[
  { productImage: StaticProductImage},
  { productImage: StaticProductImage1},
  {productImage: StaticProductImage2},
  { productImage: StaticProductImage3},
]


export default function HomeScreen(){
    return (
 
  <SafeAreaView style={styles.container}>
     <Slide></Slide>
    <FlatList 
    style={styles.FlatList}
    numColumns={2}
    data={products}
    
    renderItem={( { item}) => (<Product product={item}/>)}
    >
    </FlatList>
    
    
  </SafeAreaView>
)
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent:'center',
    alignItems: 'center'
    
  },
  FlatList:{
    width:'100%',
   
    
  },
});
