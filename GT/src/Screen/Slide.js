
import React, {useState} from 'react';
import { 
  StyleSheet, 
  Text, 
  Dimensions,
  View,
  StatusBar,
  SafeAreaView,
  Image

} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Background from '../components/Background';


const images = [
  'https://cdn.pixabay.com/photo/2018/02/22/14/58/nature-3173180_1280.jpg',
  'https://cdn.pixabay.com/photo/2018/06/07/09/01/emotions-3459666_1280.jpg',
  'https://cdn.pixabay.com/photo/2016/03/04/19/36/beach-1236581_1280.jpg'
]

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default function Slide() {
  const [imgActive, setingActive] = useState(0);

  onchange = (nativeEvent) => {
    if(nativeEvent) {
      const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width)
      if(slide != imgActive){
        setingActive(slide);
      }
    }
  }
  

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.wrap}>
        <ScrollView
          onScroll={({nativeEvent}) => onchange(nativeEvent)}
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          horizontal
          style={styles.wrap}
        >
          {
            images.map((e, index)=>
            <Image
             key={e}
             resizeMode='stretch'
             style={styles.wrap}
             source={{uri: e}}
             />
            )
          }

        </ScrollView>
        <View style={styles.wrapDot}>
          {
            images.map((e, index) =>
            <Text key={e} 
            style={imgActive == index ? styles.dotActive : styles.dot}
            >
              .
            </Text>
          )
          }
        </View>
      </View>
    </SafeAreaView>
  )
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center',
    margin:'28%',
    paddingBottom:'2%'
    
  },
  wrap: {
    width: WIDTH,
    height: HEIGHT * 0.30
  },
  wrapDot: {
    position :'absolute',
    bottom:0,
    flexDirection:'row',
    alignSelf:'center'

  },
  dotActive: {
    margin:3,
    color: 'black' 
  },
  dot: {
    margin:3,
    color: 'white'
  }
});


