import React from "react";
import { View, StyleSheet } from "react-native";
import { DrawerItem, DrawerContentScrollView } from "@react-navigation/drawer";
import { 
    Avatar,
    Drawer,
    Switch,
    Text,
} from "react-native-paper";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


export default function Drawerjs() {
    return (
        <DrawerContentScrollView>
            <View style={styles.drawerContent}>
                <View style={styles.userInfoSection}>
                    <Avatar.Image
                    size={80}
                    source={
                        require("../../assets/luffy.png")
                    }
                    />
                </View>

                <Drawer.Section style={styles.drawerSection}>
                    <DrawerItem
                    icon={({color, size}) => (
                        <Icon
                        name="home-outline"
                        color={color}
                        size={size}
                        />
                    )}
                    label="Home"
                    onPress= {() => {navigation.navigate('HomeScreen')}}
                    />

                    <DrawerItem
                    icon={({color, size}) => (
                        <Icon
                        name="post-outline"
                        color={color}
                        size={size}
                        />
                    )}
                    label="Post"
                    onPress= {() => {}}
                    />
                    <DrawerItem
                    icon={({color, size}) => (
                        <Icon
                        name="message-outline"
                        color={color}
                        size={size}
                        />
                    )}
                    label="Feedback"
                    onPress= {() => {}}
                    />
                    <DrawerItem
                    icon={({color, size}) => (
                        <Icon
                        name="phone-outline"
                        color={color}
                        size={size}
                        />
                    )}
                    label="Contact us"
                    onPress= {() => {}}
                    />
                </Drawer.Section>
             
                <Drawer.Section style={styles.drawerSection}>
                    <DrawerItem
                    label="Settings"
                    onPress= {() => {}}
                    />
                </Drawer.Section>
            </View>
        </DrawerContentScrollView>
    )
}


const styles =StyleSheet.create({
    drawerContent: {
        flex:1,
    },
    userInfoSection:{
        paddingLeft:100,
    },
    
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight:15,
    },
    drawerSection: {
        marginTop:15,
    },
})