import React from "react";
import Header from "../components/header";
import Background from "../components/Background";

export default function Hardcopy({navigation}){
    return (
        <Background>
            <Header> HardCopy</Header>
        </Background>
    )
}